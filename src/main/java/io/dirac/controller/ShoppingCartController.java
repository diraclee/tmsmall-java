package io.dirac.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.dirac.entity.ShoppingCart;
import io.dirac.result.Result;
import io.dirac.service.ShoppingCartService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
@RestController
@RequestMapping("/shopping-cart")
public class ShoppingCartController {

	@Autowired
	private ShoppingCartService shoppingCartService;

	@PostMapping("/add-item")
	public Result addItem(@Validated @RequestBody ShoppingCart item) {
		shoppingCartService.save(item);
		return Result.success("OK");
	}

	@GetMapping("/{user_id}")
	public Result getItemByUserId(@PathVariable(value = "user_id") Long user_id) {
		QueryWrapper<ShoppingCart> wrapper = new QueryWrapper<>();
		wrapper.eq("user_id", user_id);
		return Result.success(shoppingCartService.list(wrapper));
	}

	@PutMapping("/update-batch")
	public Result updateItem(@Validated @RequestBody List<ShoppingCart> items) {
		shoppingCartService.saveOrUpdateBatch(items);
		return Result.success("OK");
	}

	@DeleteMapping("/delete-batch")
	public Result deleteItems(@RequestBody List<Long> ids) {
		shoppingCartService.removeByIds(ids);
		return Result.success("OK");
	}

	@DeleteMapping("/delete")
	public Result deleteItem(@RequestBody Long id) {
		shoppingCartService.removeById(id);
		return Result.success("OK");
	}
}
