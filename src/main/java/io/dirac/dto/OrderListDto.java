package io.dirac.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderListDto {

    private Long orderId;

    private String userName;

    private String productName;

    private String productImgPath;

    private BigDecimal productPrice;

    private BigDecimal purchaseAmount;

    private BigDecimal productStock;

    private String orderStatus;
}
