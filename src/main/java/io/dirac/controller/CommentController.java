package io.dirac.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
@RestController
@RequestMapping("/comment")
public class CommentController {

}
