package io.dirac.controller;


import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.dirac.entity.ProductCategory;
import io.dirac.result.Result;
import io.dirac.service.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
@RestController
@RequestMapping("/product-category")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @GetMapping("/species")
    public Result getSpecies() {
        QueryWrapper<ProductCategory> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", 0L);
        List<ProductCategory> species = productCategoryService.list(wrapper);
        return Result.success(species);
    }

    @GetMapping("/brands")
    public Result getBrands() {
        QueryWrapper<ProductCategory> wrapper = new QueryWrapper<>();
        wrapper.ne("parent_id", 0L);
        List<ProductCategory> brands = productCategoryService.list(wrapper);
        return Result.success(brands);
    }

    @GetMapping("/{id}")
    public Result getById(@PathVariable(value = "id") Long id) {
        return Result.success(productCategoryService.getById(id));
    }
}
