package io.dirac.service;

import io.dirac.dto.OrderListDto;

import java.util.List;

public interface OrderListDtoService {

    public List<OrderListDto> getBatchByUserId(Long userId);
}
