package io.dirac.mapper;

import io.dirac.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
@Repository
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

}
