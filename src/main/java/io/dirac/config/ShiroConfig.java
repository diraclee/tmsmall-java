package io.dirac.config;


import io.dirac.shiro.AccountRealm;
import io.dirac.shiro.JwtFilter;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Autowired
    JwtFilter jwtFilter;

    // 定义一个 SessionManager 对象，并为其装配
    //  RedisSessionDAO 对象：该对象已被 shiro-redis 放到容器，但未被 IDEA 识别
    @Bean
    public SessionManager sessionManager(RedisSessionDAO redisSessionDAO) {
        DefaultSessionManager sessionManager = new DefaultSessionManager();
        sessionManager.setSessionDAO(redisSessionDAO);
        return sessionManager;
    }


    // 定义一个 DefaultWebSecurityManager 对象，并为其装配
    //  AccountRealm 对象：自定义 Realm 类，已声明为 Component
    //  SessionManager 对象：上文定义的 bean
    //  RedisCacheManager 对象：该对象已被 shiro-redis 放到容器，但未被 IDEA 识别
    @Bean
    public DefaultWebSecurityManager webSecurityManager(AccountRealm accountRealm,
                                                        SessionManager sessionManager,
                                                        RedisCacheManager redisCacheManager) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager(accountRealm);

        securityManager.setSessionManager(sessionManager);
        securityManager.setCacheManager(redisCacheManager);

        return securityManager;
    }

    // 定义一个 ShiroFilterChainDefinition 对象，配置哪些路由被哪些拦截器拦截
    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition filterChainDefinition = new DefaultShiroFilterChainDefinition();

        Map<String, String> filterMap = new HashMap<>();
        filterMap.put("/**", "jwt");

        filterChainDefinition.addPathDefinitions(filterMap);

        return filterChainDefinition;
    }

    // 定义一个 ShiroFilterFactoryBean 对象，并为其装配
    // SecurityManager：上文定义的 bean
    // ShiroFilterChainDefinition：上文定义的 bean
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager,
                                                         ShiroFilterChainDefinition shiroFilterChainDefinition) {
        ShiroFilterFactoryBean filterFactory = new ShiroFilterFactoryBean();
        filterFactory.setSecurityManager(securityManager);

        Map<String, Filter> filters = new HashMap<>();
        filters.put("jwt", jwtFilter);

        filterFactory.setFilters(filters);

        Map<String, String> filterMap = shiroFilterChainDefinition.getFilterChainMap();
        filterFactory.setFilterChainDefinitionMap(filterMap);

        return filterFactory;
    }

    // 开启注解支持
    // SecurityManager：上文定义的 bean
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
    @Bean
    public static DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator() {
        return new DefaultAdvisorAutoProxyCreator();
    }
}