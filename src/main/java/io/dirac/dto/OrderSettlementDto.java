package io.dirac.dto;

import io.dirac.entity.OrderDetail;
import io.dirac.entity.OrderSurvey;
import lombok.Data;

import java.util.List;

@Data
public class OrderSettlementDto {

    private OrderSurvey orderSurvey;

    private List<OrderDetail> orderDetails;

    private List<Long> cartItemIds;
}
