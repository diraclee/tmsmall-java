package io.dirac.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.dirac.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;


    @Test
    public void test() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", 1381230808806350850L).set("score", 10.0);
        userService.update(updateWrapper);
    }
}
