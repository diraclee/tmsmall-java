package io.dirac.mapper;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.dirac.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testUpdateById() {
        User user = userMapper.selectById(1381230808806350850L);
        userMapper.updateById(user);
    }

}
