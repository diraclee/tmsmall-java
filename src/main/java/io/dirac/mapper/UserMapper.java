package io.dirac.mapper;

import io.dirac.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Dirac
 * @since 2021-04-27
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
