package io.dirac.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.dirac.entity.User;
import io.dirac.mapper.UserMapper;
import io.dirac.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-27
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public void increaseScore(Long userId, BigDecimal score) {
        User user = userMapper.selectById(userId);
        System.out.println(user.getUsername() + " increased score by" + score.floatValue());
        user.setScore(user.getScore().add(score));
        userMapper.updateById(user);
    }

    @Override
    public void decreaseScore(Long userId, BigDecimal score) {
        User user = userMapper.selectById(userId);
        user.setScore(user.getScore().subtract(score));
        userMapper.updateById(user);
    }
}
