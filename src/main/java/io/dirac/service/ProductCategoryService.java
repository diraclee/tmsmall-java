package io.dirac.service;

import io.dirac.entity.ProductCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
public interface ProductCategoryService extends IService<ProductCategory> {

}
