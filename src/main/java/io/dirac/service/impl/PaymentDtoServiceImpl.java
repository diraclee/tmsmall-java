package io.dirac.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.dirac.dto.PaymentDto;
import io.dirac.entity.OrderDetail;
import io.dirac.entity.OrderSurvey;
import io.dirac.mapper.OrderDetailMapper;
import io.dirac.mapper.OrderSurveyMapper;
import io.dirac.result.Result;
import io.dirac.service.PaymentDtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentDtoServiceImpl implements PaymentDtoService {

    @Autowired
    private OrderSurveyMapper orderSurveyMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;


    @Override
    public PaymentDto getByOrderId(Long orderId) {
        OrderSurvey orderSurvey = orderSurveyMapper.selectById(orderId);
        QueryWrapper<OrderDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id", orderId);
        List<OrderDetail> orderDetails = orderDetailMapper.selectList(wrapper);
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setOrderSurvey(orderSurvey);
        paymentDto.setOrderDetails(orderDetails);
        return paymentDto;
    }
}
