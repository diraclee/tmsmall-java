package io.dirac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmsmallJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmsmallJavaApplication.class, args);
    }

}
