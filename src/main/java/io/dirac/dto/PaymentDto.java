package io.dirac.dto;

import io.dirac.entity.OrderDetail;
import io.dirac.entity.OrderSurvey;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PaymentDto {

    OrderSurvey orderSurvey;

    List<OrderDetail> orderDetails;
}
