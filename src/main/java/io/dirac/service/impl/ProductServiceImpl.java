package io.dirac.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.dirac.dto.SalesDto;
import io.dirac.entity.Product;
import io.dirac.mapper.ProductMapper;
import io.dirac.service.ProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<SalesDto> getSales() {
        return productMapper.getSales();
    }
}
