package io.dirac.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.dirac.dto.SalesDto;
import io.dirac.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;
import javafx.scene.control.Pagination;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
public interface ProductService extends IService<Product> {
    List<SalesDto> getSales();
}
