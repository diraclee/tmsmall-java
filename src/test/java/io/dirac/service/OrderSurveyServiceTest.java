package io.dirac.service;

import io.dirac.entity.OrderSurvey;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderSurveyServiceTest {

    @Autowired
    private OrderSurveyService orderSurveyService;

    @Test
    public void test() {
        OrderSurvey orderSurvey = orderSurveyService.getById(1384430347344355330L);
        orderSurveyService.saveOrUpdate(orderSurvey);
    }
}
