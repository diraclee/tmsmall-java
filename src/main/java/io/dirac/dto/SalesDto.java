package io.dirac.dto;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class SalesDto implements Serializable {

    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private BigDecimal stock;

    private Long pcId;

    private Long pcChildId;

    private String imgPath;

    private Integer version;

    private Integer deleted;

    private Date gmtCreated;

    private Date gmtModified;

    private BigDecimal salesVolume;
}
