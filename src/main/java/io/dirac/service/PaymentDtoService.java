package io.dirac.service;

import io.dirac.dto.PaymentDto;

public interface PaymentDtoService {

    PaymentDto getByOrderId(Long orderId);
}
