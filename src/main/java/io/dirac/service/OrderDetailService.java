package io.dirac.service;

import io.dirac.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-12
 */
public interface OrderDetailService extends IService<OrderDetail> {

    public List<OrderDetail> getByOrderId(Long orderId);
}
