package io.dirac.mapper;

import io.dirac.entity.OrderSurvey;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Dirac
 * @since 2021-04-14
 */
@Repository
public interface OrderSurveyMapper extends BaseMapper<OrderSurvey> {

}
