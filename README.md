# Thomas Mall

复旦大学计算机学院软件过程管理硕士课程项目 Thomas Mall 后端部分。

## 环境：
- Java 1.8
- Maven 3.6.3
- MySQL x64 5.7.32
- Redis x64 5.0.10

## 接口
- dev 8082
- release 8082

## 新功能

### 积分（已完成）
- 每花费 1 元增加 1 个积分
- 每 1000 个积分折扣现金 1 元

> user 表需要增加 score 字段

### 所购商品状态
- 对接物流系统，查看所购商品状态

需求问题：
对接物流系统后，商品状态如何判定？
什么时候是待发货状态？
什么时候是待接收状态？
什么时候是已完成状态？

> order_detail 表需要增加 status 字段
> - 待支付 0
> - 待发货 1
> - 待接收 2
> - 待评价 3
> - 已完成 4
> order_survey 表 status 字段仍保留
> - 待支付 0
> - 已支付 1

### 退货
- 用户可以点击退货取消订单

需求问题：
已付款的订单可以取消吗？
取消订单允许单个商品取消吗？

### 供应商账号
- 供应商可以提供自己的商品上架销售



### 促销活动
- 两件9折

需求问题：
哪些商品两件 9 折？所有的吗？

## 接口设计

### 商品列表
- id
- name
- description
- price
- stock
- speciesId
- brandId
- imgPath

### 商品详情

#### 商品详情展示
- id
- name
- description
- price
- stock
- speciesId
- brandId
- imgPath

#### 加入购物车
- productId
- productName
- productImgPath
- productPrice
- purchaseQuantity
- productStock
- userId

### 购物车

#### 购物车展示
- cartItemId
- productId
- productName
- productImgPath
- productPrice
- purchase (purchaseQuantity)
- productStock
- userId

#### 结算

#### 存储一个订单概览
- userId
- username
- userAddress
- totalPrice
- status
- type

#### 存储多个订单详情项
- productId
- productCount (purchaseQuantity)
- productPrice

#### 删除购物车项
- cartItemId

### 支付
- orderId
- userName
- purchaseDetail
- paymentAmount

### purchaseDetail
- productName
- purchaseQuantity