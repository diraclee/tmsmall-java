package io.dirac;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CodeGenerator {
    public static void main(String[] args) {

        AutoGenerator mpg = new AutoGenerator();

        // 1、全局配置
        GlobalConfig gconfig = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gconfig.setOutputDir(projectPath+"/src/main/java");
        gconfig.setAuthor("Dirac");
        gconfig.setOpen(false);  // 是否打开文件资源管理器
        gconfig.setFileOverride(true);  // 是否覆盖原文件
        gconfig.setServiceName("%sService");  // 去掉 Service 的 I 前缀
        gconfig.setIdType(IdType.ASSIGN_ID);
        gconfig.setDateType(DateType.ONLY_DATE);
        gconfig.setSwagger2(true);

        mpg.setGlobalConfig(gconfig);

        // 2、设置数据源[===============按需修改===================]
        DataSourceConfig dsconfig = new DataSourceConfig();
        dsconfig.setUrl("jdbc:mysql://localhost:3306/tms_mall?useSSL=true&useUnicode=true&characterEncoding=UTF-8&serverTimeZone=GMT%2B8");
        dsconfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dsconfig.setUsername("root");
        dsconfig.setPassword("123456");

        dsconfig.setDbType(DbType.MYSQL);

        mpg.setDataSource(dsconfig);

        // 3、包的配置[===============按需修改===================]
        PackageConfig pconfig = new PackageConfig();
        pconfig.setModuleName("");  // 子模块包名

        // 项目包名[===============按需修改===================]
        pconfig.setParent("io.dirac");

        pconfig.setEntity("entity");
        pconfig.setMapper("mapper");
        pconfig.setService("service");
        pconfig.setController("controller");

        mpg.setPackageInfo(pconfig);

        // 4、策略配置
        StrategyConfig strategy = new StrategyConfig();

        // 映射 user 表和 book 表[===============按需修改===================]
//        strategy.setInclude("comment", "order", "order_detail", "product", "product_category", "shopping_cart", "user");
        strategy.setInclude("user");

        strategy.setNaming(NamingStrategy.underline_to_camel);  // 表名自动转换：蛇名=>驼峰名
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);  // 字段名自动转换：蛇名=>驼峰名
        strategy.setEntityLombokModel(true);  // 自动 Lombok
        strategy.setLogicDeleteFieldName("deleted");  // 逻辑删除字段

        // 自动填充，填充规则 MyMetaObjectHandler 仍需自己写！
        TableFill gmtCreated = new TableFill("gmt_created", FieldFill.INSERT);
        TableFill gmtModified = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);
        strategy.setTableFillList(Arrays.asList(gmtCreated, gmtModified));

        strategy.setVersionFieldName("version");  // 乐观锁

        strategy.setRestControllerStyle(true);  // RESTfull 命名风格
        strategy.setControllerMappingHyphenStyle(true);  // RESTfull 下划线命名 localhost:8080/book_id_2

        mpg.setStrategy(strategy);

        // 5、模板配置
        TemplateConfig templateConfig = new TemplateConfig();

        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 7、模板引擎配置
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        // 6、自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };



        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();

        // 自定义配置会被优先输出
        // 如果模板引擎是 freemarker
        // String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // TODO: 按需修改，与 application.xml 中的 mapper-locations 一致，默认是在 mapper 接口下的子目录中
                return projectPath + "/src/main/resources/mapper/"
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);


        mpg.execute();
    }
}