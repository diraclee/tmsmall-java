package io.dirac.service;

import io.dirac.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-27
 */
public interface UserService extends IService<User> {

    void increaseScore(Long userId, BigDecimal score);

    void decreaseScore(Long userId, BigDecimal score);
}
