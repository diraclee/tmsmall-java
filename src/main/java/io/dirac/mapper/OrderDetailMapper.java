package io.dirac.mapper;

import io.dirac.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Dirac
 * @since 2021-04-12
 */
@Repository
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
