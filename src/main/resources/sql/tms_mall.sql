/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50732
Source Host           : localhost:3306
Source Database       : tms_mall

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2021-06-15 14:47:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '评论ID',
  `content` varchar(200) CHARACTER SET utf8 NOT NULL COMMENT '评论内容',
  `create_time` date NOT NULL COMMENT '评论日期',
  `reply` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '回复内容',
  `reply_time` date DEFAULT NULL COMMENT '回复日期',
  `nick_name` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '评论者网名',
  `version` int(4) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(4) NOT NULL COMMENT '逻辑删除',
  `gmt_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('9', '好久不见你好吗', '2019-01-04', '我很想你啊', '2019-01-04', '蜕变', '1', '0', '2021-04-02 08:05:13', '2021-04-02 08:05:13');
INSERT INTO `comment` VALUES ('10', '我要退货', '2019-01-05', '', null, '肖箫', '1', '0', '2021-04-02 08:05:13', '2021-04-02 08:05:13');
INSERT INTO `comment` VALUES ('11', '你家的东西什么玩意啊，无缘无故闹肚子', '2019-01-05', '亲！请检查您的肚子，或者购买其他零食进行服用，还可以过来托马斯105，让店主给你揉揉哦~', '2019-01-05', '箫荷', '1', '0', '2021-04-02 08:05:13', '2021-04-02 08:05:13');
INSERT INTO `comment` VALUES ('12', '???????~', '2021-03-25', '', null, '??', '1', '0', '2021-04-02 08:05:13', '2021-04-02 08:05:13');
INSERT INTO `comment` VALUES ('13', 'Hello every one!', '2021-03-25', '', null, 'Dirac', '1', '0', '2021-04-02 08:05:13', '2021-04-02 08:05:13');

-- ----------------------------
-- Table structure for `order_detail`
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` bigint(32) NOT NULL COMMENT '订单描述ID',
  `order_id` bigint(32) NOT NULL COMMENT '订单ID',
  `product_id` bigint(32) NOT NULL COMMENT '商品ID',
  `product_name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '商品名',
  `purchase_quantity` decimal(32,0) NOT NULL COMMENT '购买商品个数',
  `product_price` decimal(32,2) NOT NULL COMMENT '商品总价',
  `version` int(2) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '逻辑删除',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_detail
-- ----------------------------
INSERT INTO `order_detail` VALUES ('1386942780113891329', '1386942779954507778', '31', '上好佳薯片', '1', '6.00', '1', '0', '2021-04-27 15:18:22', '2021-04-27 15:18:22');
INSERT INTO `order_detail` VALUES ('1386943834041823233', '1386943833924382721', '20', '周黑鸭 卤凤爪', '1', '23.00', '1', '0', '2021-04-27 15:22:33', '2021-04-27 15:22:33');
INSERT INTO `order_detail` VALUES ('1397149169184608257', '1397149159999082497', '31', '上好佳薯片', '1', '6.00', '1', '0', '2021-05-25 19:14:54', '2021-05-25 19:14:54');
INSERT INTO `order_detail` VALUES ('1397471731017981954', '1397471730598551554', '31', '上好佳薯片', '1', '6.00', '1', '0', '2021-05-26 16:36:39', '2021-05-26 16:36:39');
INSERT INTO `order_detail` VALUES ('1397472927623553025', '1397472927296397313', '31', '上好佳薯片', '1', '6.00', '1', '0', '2021-05-26 16:41:24', '2021-05-26 16:41:24');
INSERT INTO `order_detail` VALUES ('1404682375588888577', '1404682374825525250', '31', '上好佳薯片', '1', '6.00', '1', '0', '2021-06-15 14:09:11', '2021-06-15 14:09:11');
INSERT INTO `order_detail` VALUES ('1404684855680843777', '1404684855538237442', '14', '葵花籽奶油味瓜子', '1', '10.00', '1', '0', '2021-06-15 14:19:02', '2021-06-15 14:19:02');
INSERT INTO `order_detail` VALUES ('1404684855689232385', '1404684855538237442', '21', '康师傅方便面泡面', '2', '40.00', '1', '0', '2021-06-15 14:19:02', '2021-06-15 14:19:02');
INSERT INTO `order_detail` VALUES ('1404684948731478018', '1404684948584677377', '17', '香辣味面筋辣条', '1', '14.00', '1', '0', '2021-06-15 14:19:24', '2021-06-15 14:19:24');
INSERT INTO `order_detail` VALUES ('1404686036134150145', '1404686035987349505', '14', '葵花籽奶油味瓜子', '1', '10.00', '1', '0', '2021-06-15 14:23:43', '2021-06-15 14:23:43');
INSERT INTO `order_detail` VALUES ('1404688374857080833', '1404688374508953601', '16', '口水娃辣条辣片', '1', '13.00', '1', '0', '2021-06-15 14:33:01', '2021-06-15 14:33:01');
INSERT INTO `order_detail` VALUES ('1404688698875551745', '1404688698686808065', '14', '葵花籽奶油味瓜子', '1', '10.00', '1', '0', '2021-06-15 14:34:18', '2021-06-15 14:34:18');
INSERT INTO `order_detail` VALUES ('1404691615854559234', '1404691615569346562', '14', '葵花籽奶油味瓜子', '1', '10.00', '1', '0', '2021-06-15 14:45:54', '2021-06-15 14:45:54');
INSERT INTO `order_detail` VALUES ('1404691615875530754', '1404691615569346562', '17', '香辣味面筋辣条', '1', '14.00', '1', '0', '2021-06-15 14:45:54', '2021-06-15 14:45:54');

-- ----------------------------
-- Table structure for `order_survey`
-- ----------------------------
DROP TABLE IF EXISTS `order_survey`;
CREATE TABLE `order_survey` (
  `id` bigint(32) NOT NULL COMMENT '订单ID',
  `user_id` bigint(32) NOT NULL COMMENT '买家ID',
  `user_name` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '买家姓名',
  `user_address` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '买家收货地址',
  `total_price` decimal(16,2) NOT NULL COMMENT '订单价格',
  `discount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '折扣',
  `payment_amount` decimal(16,2) NOT NULL COMMENT '实付金额',
  `status` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '订单状态(待支付/已支付)',
  `type` int(4) NOT NULL COMMENT '订单类型',
  `version` int(4) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除',
  `gmt_created` datetime NOT NULL COMMENT '订单创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '订单修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of order_survey
-- ----------------------------
INSERT INTO `order_survey` VALUES ('1386942779954507778', '1381230808806350850', 'Dirac', '上海滩', '6.00', '0.00', '6.00', '已支付', '1', '1', '0', '2021-04-27 15:18:22', '2021-04-27 15:18:22');
INSERT INTO `order_survey` VALUES ('1386943833924382721', '1381230808806350850', 'Dirac', '上海滩', '23.00', '1.00', '22.00', '已支付', '1', '1', '0', '2021-04-27 15:22:33', '2021-04-27 15:22:33');
INSERT INTO `order_survey` VALUES ('1397149159999082497', '1381230808806350850', 'Dirac', '上海滩', '6.00', '0.00', '6.00', '已支付', '1', '1', '0', '2021-05-25 19:14:52', '2021-05-25 19:14:52');
INSERT INTO `order_survey` VALUES ('1397471730598551554', '1381230808806350850', 'Dirac', '上海滩', '6.00', '2.00', '4.00', '已支付', '1', '1', '0', '2021-05-26 16:36:39', '2021-05-26 16:36:39');
INSERT INTO `order_survey` VALUES ('1397472927296397313', '1381230808806350850', 'Dirac', '上海滩', '6.00', '2.00', '4.00', '已支付', '1', '1', '0', '2021-05-26 16:41:24', '2021-05-26 16:41:24');
INSERT INTO `order_survey` VALUES ('1404682374825525250', '1381230808806350850', 'Dirac', '上海滩', '6.00', '5.00', '1.00', '已支付', '1', '1', '0', '2021-06-15 14:09:11', '2021-06-15 14:09:11');
INSERT INTO `order_survey` VALUES ('1404684855538237442', '1381230808806350850', 'Dirac', '上海滩', '90.00', '2.00', '88.00', '已支付', '1', '1', '0', '2021-06-15 14:19:02', '2021-06-15 14:19:02');
INSERT INTO `order_survey` VALUES ('1404684948584677377', '1381230808806350850', 'Dirac', '上海滩', '14.00', '1.00', '13.00', '已支付', '1', '1', '0', '2021-06-15 14:19:24', '2021-06-15 14:19:24');
INSERT INTO `order_survey` VALUES ('1404686035987349505', '1381230808806350850', 'Dirac', '上海滩', '10.00', '1.00', '9.00', '已支付', '1', '1', '0', '2021-06-15 14:23:43', '2021-06-15 14:23:43');
INSERT INTO `order_survey` VALUES ('1404688374508953601', '1381230808806350850', 'Dirac', '上海滩', '13.00', '1.00', '12.00', '已支付', '1', '1', '0', '2021-06-15 14:33:01', '2021-06-15 14:33:01');
INSERT INTO `order_survey` VALUES ('1404688698686808065', '1381230808806350850', 'Dirac', '上海滩', '10.00', '1.00', '9.00', '已支付', '1', '1', '0', '2021-06-15 14:34:18', '2021-06-15 14:34:18');
INSERT INTO `order_survey` VALUES ('1404691615569346562', '1381230808806350850', 'Dirac', '上海滩', '24.00', '6.00', '18.00', '已支付', '1', '1', '0', '2021-06-15 14:45:54', '2021-06-15 14:45:54');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` bigint(32) NOT NULL COMMENT '商品ID',
  `name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '商品名',
  `description` varchar(512) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品描述',
  `supplier_id` bigint(32) NOT NULL DEFAULT '0' COMMENT '供应商ID',
  `price` decimal(32,2) NOT NULL COMMENT '商品价格',
  `stock` decimal(32,0) NOT NULL COMMENT '商品库存量',
  `pc_id` bigint(32) DEFAULT NULL COMMENT '商品类型ID',
  `pc_child_id` bigint(32) DEFAULT NULL COMMENT '商品类型子ID',
  `img_path` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '图片文件名',
  `version` int(4) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('13', '坚果炒货西瓜子', '坚果炒货西瓜子 颗粒饱满休闲零食小吃 话梅西瓜子200g/袋', '0', '18.00', '21', '27', '29', 'q2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('14', '葵花籽奶油味瓜子', '三只松鼠坚果炒货休闲零食葵花籽奶油味瓜子168g/袋', '0', '10.00', '34', '27', '30', 'q3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('15', '卫龙辣条小面筋', '卫龙 辣条 休闲零食 方便食品 小面筋量贩装（香辣味）312g/袋', '0', '12.00', '22', '31', '32', 'l1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('16', '口水娃辣条辣片', '口水娃 年货休闲零食 辣条辣片 即食零食小吃400g', '0', '13.00', '4', '31', '33', 'l2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('17', '香辣味面筋辣条', '良品铺子_烤面筋200g辣皮辣片辣条味即食零食小吃 香辣味面筋辣条零食 烤面筋 200g', '0', '14.00', '117', '31', '34', 'l3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('18', '泡椒凤爪', '有友 【第二份五折】泡椒凤爪428g袋独立小包装 鸡爪 重庆特产 休闲零食 【山椒味】辣度★★★★', '0', '36.00', '100', '35', '36', 'p1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('19', '冷冻鸡凤爪', '泰森(Tyson) 冷冻鸡凤爪 454g/袋 2袋装', '0', '56.00', '35', '35', '37', 'p2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('20', '周黑鸭 卤凤爪', '周黑鸭 卤凤爪 武汉特产卤味零食麻辣小吃鸡爪 真空小包装150g', '0', '23.00', '24', '35', '38', 'p3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('21', '康师傅方便面泡面', '康师傅方便面泡面 日式豚骨2桶+酸酸辣辣豚骨2桶+海鲜豚骨2桶+藤椒豚骨2桶 桶装泡面整箱装', '0', '40.00', '23', '39', '40', 'k1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('22', '统一 方便面泡面', '统一 方便面泡面 汤达人 日式豚骨味方便面 五连包', '0', '21.00', '36', '39', '41', 'k2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('23', '今麦郎 方便面泡面 ', '今麦郎 方便面泡面 大今野拉面 红烧牛肉面 24袋 泡面整箱装', '0', '35.00', '130', '39', '42', 'k3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('24', '碗装醇黑巧克力糖果', '德芙 Dove分享碗装醇黑巧克力66% 糖果巧克力 休闲零食 252g', '0', '39.00', '52', '43', '44', 'y1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('25', '夹心喜糖巧克力糖果', '徐福记 奇欧比巧克力 散装软馅糖果零食 夹心喜糖巧克力批发500g 多种口味混合500g约45颗', '0', '39.00', '23', '43', '45', 'y2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('26', '费列罗巧克力礼盒糖果', '费列罗巧克力礼盒装生日礼物情人节礼物送女友年货节糖果员工福利', '0', '89.00', '96', '43', '46', 'y3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('27', ' 黑加仑葡萄干', '好想你 休闲零食 新疆特产 蜜饯果干 黑加仑葡萄干228g/袋', '0', '18.00', '62', '47', '48', 'h1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('28', '玫瑰红黑加仑金凰后无核白葡萄干', '楼兰蜜语 蜜饯果干 玫瑰红黑加仑金凰后无核白葡萄干 新疆特产零食四色大葡萄干900g', '0', '34.00', '42', '47', '49', 'h2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('29', '玫瑰红葡萄干', '西域美农 休闲零食 蜜饯果干 新疆特产 食品小吃 玫瑰红葡萄干250g', '0', '15.00', '20', '47', '50', 'h3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('30', '乐事薯片', ' 乐事薯片超值分享装210g', '0', '15.00', '72', '51', '52', 's1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('31', '上好佳薯片', '上好佳薯片组合包16g*5袋', '0', '6.00', '196', '51', '53', 's2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('32', '可比克 薯片', '可比克 薯片 心有所薯 办公室休闲膨化零食 105g*3 组合装（番茄味+烧烤味+原味)', '0', '17.00', '56', '51', '54', 's3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('33', '口味硬糖棒棒糖', '阿尔卑斯 精选多种口味硬糖棒棒糖(20支装)200克牛奶糖 儿童用糖 休闲零食(新老包装交替发货)', '0', '10.00', '262', '55', '56', 't1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('34', '绿箭无糖薄荷糖口香糖', '绿箭无糖薄荷糖口香糖铁盒装23.8g/盒 约35粒 休闲零食糖果 冰柠薄荷味', '0', '7.00', '40', '55', '57', 't2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('35', '大白兔 原味奶糖', '大白兔 原味奶糖 454g/袋 婚庆喜糖', '0', '24.00', '130', '55', '58', 't3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('36', '猪肉脯', '来伊份 【前1500名第2份5折】原味/芝麻味猪肉干 猪肉脯猪200g来一份休闲零食 原味猪肉脯200g', '0', '20.00', '34', '59', '60', 'r1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('37', '蜀道香 肉干肉脯', '蜀道香 肉干肉脯 休闲零食特产 手撕牛肉 独立小包装 天椒霸道麻辣牛肉干200g', '0', '33.00', '45', '59', '61', 'r2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('38', '老四川肉干肉脯', '金角 老四川 休闲零食 肉干肉脯 五香牛肉干208g中华老字号', '0', '49.00', '20', '59', '62', 'r3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('39', '伊利 纯牛奶', ' 伊利 纯牛奶250ml*24盒', '0', '66.00', '176', '63', '64', 'n1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('40', '低脂高钙牛奶', '蒙牛 低脂高钙牛奶 250ml*16 礼盒装', '0', '45.00', '57', '63', '65', 'n2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('41', '旺仔 牛奶 ', '沃尔玛】旺仔 牛奶 儿童牛奶 早餐奶 3L 125ml*24包', '0', '52.00', '56', '63', '66', 'n3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('42', '广东特产鸡蛋卷', '元朗 鸡蛋卷 饼干糕点 礼盒送礼 广东特产680g', '0', '66.00', '45', '67', '68', 'z1.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('43', '广东特产广式饼酥糕点', '广州酒家 金装手信礼盒424g/盒 广东特产广式饼酥糕点下午茶点心 中华老字号手信', '0', '66.00', '44', '67', '69', 'z2.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('44', '广东广州特产传统糕点', '莲香楼杏仁饼铁盒400g点心广东广州特产传统糕点下午茶点休闲零食送礼手信礼盒', '0', '46.00', '79', '67', '70', 'z3.jpg', '1', '0', '2021-03-25 15:13:37', '2021-03-25 15:13:37');
INSERT INTO `product` VALUES ('1397108616707379201', 'LevelDB', 'A great DB.', '7', '100.00', '1000', '27', '28', 'https://gitee.com/DiracLee/picbed/raw/master/img/20210512154233.png', '1', '0', '2021-05-25 16:33:46', '2021-05-25 16:33:46');
INSERT INTO `product` VALUES ('1397144748069425153', 'OK', 'OK图标', '1397119091889344513', '1.00', '12', '22', '31', 'https://gitee.com/DiracLee/picbed/raw/master/img/ok.png', '1', '0', '2021-05-25 18:57:20', '2021-05-25 18:57:20');

-- ----------------------------
-- Table structure for `product_category`
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `id` bigint(32) NOT NULL COMMENT '商品类型ID',
  `name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '商品类型名',
  `parent_id` bigint(32) NOT NULL COMMENT '商品类型父ID',
  `version` int(4) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('27', '瓜子', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('28', '洽洽', '27', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('29', '百草味', '27', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('30', '三只松鼠', '27', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('31', '辣条', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('32', '卫龙', '31', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('33', '口水娃', '31', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('34', '良品铺子', '31', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('35', '凤爪', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('36', '有友', '35', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('37', '泰森', '35', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('38', '周黑鸭', '35', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('39', '泡面', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('40', '康师傅', '39', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('41', '统一', '39', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('42', '今麦郎', '39', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('43', '巧克力', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('44', '德芙', '43', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('45', '徐福记', '43', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('46', '费列罗', '43', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('47', '葡萄干', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('48', '好想你·', '47', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('49', '楼兰蜜语', '47', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('50', '西域美农', '47', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('51', '薯片', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('52', '乐事', '51', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('53', '上好佳', '51', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('54', '可比克', '51', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('55', '糖果', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('56', '阿尔卑斯', '55', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('57', '绿箭', '55', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('58', '大白兔', '55', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('59', '肉脯', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('60', '来伊份', '59', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('61', '蜀道香', '59', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('62', '老四川', '59', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('63', '牛奶', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('64', '伊利', '63', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('65', '蒙牛', '63', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('66', '沃尔玛', '63', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('67', '特产', '0', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('68', '元朗', '67', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('69', '广州酒家', '67', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');
INSERT INTO `product_category` VALUES ('70', '莲香楼', '67', '1', '0', '2021-03-25 15:16:20', '2021-03-25 15:16:20');

-- ----------------------------
-- Table structure for `shopping_cart`
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart` (
  `id` bigint(32) NOT NULL COMMENT '购物车ID',
  `product_id` bigint(32) DEFAULT NULL COMMENT '购物车商品ID',
  `product_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '购物车商品名',
  `product_img_path` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '购物车商品图片文件名',
  `product_price` decimal(32,0) DEFAULT NULL COMMENT '购物车商品单价',
  `product_stock` decimal(32,0) DEFAULT NULL COMMENT '购物车商品存量',
  `purchase_quantity` decimal(32,0) DEFAULT NULL COMMENT '购买商品数量',
  `user_id` bigint(32) DEFAULT NULL COMMENT '购物车买家ID',
  `version` int(4) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(4) NOT NULL DEFAULT '0' COMMENT '是否删除或完成',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------
INSERT INTO `shopping_cart` VALUES ('1384429406150873090', '14', '葵花籽奶油味瓜子', 'q3.jpg', '10', '34', '1', '1381230808806350850', '1', '1', '2021-04-20 16:51:07', '2021-04-20 16:51:07');
INSERT INTO `shopping_cart` VALUES ('1384429421518802945', '23', '今麦郎 方便面泡面 ', 'k3.jpg', '35', '130', '3', '1381230808806350850', '1', '1', '2021-04-20 16:51:10', '2021-04-20 16:51:10');
INSERT INTO `shopping_cart` VALUES ('1384429439248125954', '25', '夹心喜糖巧克力糖果', 'y2.jpg', '39', '23', '1', '1381230808806350850', '1', '1', '2021-04-20 16:51:14', '2021-04-20 16:51:14');
INSERT INTO `shopping_cart` VALUES ('1384433685041455105', '20', '周黑鸭 卤凤爪', 'p3.jpg', '23', '24', '1', '1381230808806350850', '1', '1', '2021-04-20 17:08:07', '2021-04-20 17:08:07');
INSERT INTO `shopping_cart` VALUES ('1384689133170667521', '20', '周黑鸭 卤凤爪', 'p3.jpg', '23', '24', '1', '1381230808806350850', '1', '1', '2021-04-21 10:03:10', '2021-04-21 10:03:10');
INSERT INTO `shopping_cart` VALUES ('1384689161209589762', '22', '统一 方便面泡面', 'k2.jpg', '21', '36', '1', '1381230808806350850', '1', '1', '2021-04-21 10:03:17', '2021-04-21 10:03:17');
INSERT INTO `shopping_cart` VALUES ('1384689185263923201', '25', '夹心喜糖巧克力糖果', 'y2.jpg', '39', '23', '1', '1381230808806350850', '1', '1', '2021-04-21 10:03:23', '2021-04-21 10:03:23');
INSERT INTO `shopping_cart` VALUES ('1385551140736380929', '13', '坚果炒货西瓜子', 'q2.jpg', '18', '21', '1', '1381230808806350850', '1', '1', '2021-04-23 19:08:29', '2021-04-23 19:08:29');
INSERT INTO `shopping_cart` VALUES ('1385551157161275394', '15', '卫龙辣条小面筋', 'l1.jpg', '12', '22', '1', '1381230808806350850', '1', '1', '2021-04-23 19:08:33', '2021-04-23 19:08:33');
INSERT INTO `shopping_cart` VALUES ('1385567830933147649', '18', '泡椒凤爪', 'p1.jpg', '36', '100', '1', '1381230808806350850', '1', '1', '2021-04-23 20:14:48', '2021-04-23 20:14:48');
INSERT INTO `shopping_cart` VALUES ('1385572046221250561', '13', '坚果炒货西瓜子', 'q2.jpg', '18', '21', '1', '1381230808806350850', '1', '1', '2021-04-23 20:31:33', '2021-04-23 20:31:33');
INSERT INTO `shopping_cart` VALUES ('1386880955787681793', '20', '周黑鸭 卤凤爪', 'p3.jpg', '23', '24', '1', '1381230808806350850', '1', '1', '2021-04-27 11:12:42', '2021-04-27 11:12:42');
INSERT INTO `shopping_cart` VALUES ('1386904458431705090', '20', '周黑鸭 卤凤爪', 'p3.jpg', '23', '24', '1', '1381230808806350850', '1', '1', '2021-04-27 12:46:05', '2021-04-27 12:46:05');
INSERT INTO `shopping_cart` VALUES ('1386907801229090818', '17', '香辣味面筋辣条', 'l3.jpg', '14', '117', '1', '1381230808806350850', '1', '1', '2021-04-27 12:59:22', '2021-04-27 12:59:22');
INSERT INTO `shopping_cart` VALUES ('1386924890086096898', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-04-27 14:07:16', '2021-04-27 14:07:16');
INSERT INTO `shopping_cart` VALUES ('1386932821305249794', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-04-27 14:38:47', '2021-04-27 14:38:47');
INSERT INTO `shopping_cart` VALUES ('1386937065781514241', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-04-27 14:55:39', '2021-04-27 14:55:39');
INSERT INTO `shopping_cart` VALUES ('1386943783521431553', '20', '周黑鸭 卤凤爪', 'p3.jpg', '23', '24', '1', '1381230808806350850', '1', '1', '2021-04-27 15:22:21', '2021-04-27 15:22:21');
INSERT INTO `shopping_cart` VALUES ('1397148927651418113', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-05-25 19:13:57', '2021-05-25 19:13:57');
INSERT INTO `shopping_cart` VALUES ('1397471708792365058', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-05-26 16:36:34', '2021-05-26 16:36:34');
INSERT INTO `shopping_cart` VALUES ('1397472900096335874', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-05-26 16:41:18', '2021-05-26 16:41:18');
INSERT INTO `shopping_cart` VALUES ('1404682329732562945', '31', '上好佳薯片', 's2.jpg', '6', '196', '1', '1381230808806350850', '1', '1', '2021-06-15 14:09:00', '2021-06-15 14:09:00');
INSERT INTO `shopping_cart` VALUES ('1404684506165297154', '14', '葵花籽奶油味瓜子', 'q3.jpg', '10', '34', '1', '1381230808806350850', '1', '1', '2021-06-15 14:17:39', '2021-06-15 14:17:39');
INSERT INTO `shopping_cart` VALUES ('1404684824261312513', '21', '康师傅方便面泡面', 'k1.jpg', '40', '23', '2', '1381230808806350850', '1', '1', '2021-06-15 14:18:55', '2021-06-15 14:18:55');
INSERT INTO `shopping_cart` VALUES ('1404684907912511489', '17', '香辣味面筋辣条', 'l3.jpg', '14', '117', '1', '1381230808806350850', '1', '1', '2021-06-15 14:19:14', '2021-06-15 14:19:14');
INSERT INTO `shopping_cart` VALUES ('1404684972941000705', '14', '葵花籽奶油味瓜子', 'q3.jpg', '10', '34', '1', '1381230808806350850', '1', '1', '2021-06-15 14:19:30', '2021-06-15 14:19:30');
INSERT INTO `shopping_cart` VALUES ('1404688354523095042', '16', '口水娃辣条辣片', 'l2.jpg', '13', '4', '1', '1381230808806350850', '1', '1', '2021-06-15 14:32:56', '2021-06-15 14:32:56');
INSERT INTO `shopping_cart` VALUES ('1404688669746110465', '14', '葵花籽奶油味瓜子', 'q3.jpg', '10', '34', '1', '1381230808806350850', '1', '1', '2021-06-15 14:34:11', '2021-06-15 14:34:11');
INSERT INTO `shopping_cart` VALUES ('1404689158235676674', '14', '葵花籽奶油味瓜子', 'q3.jpg', '10', '34', '1', '1381230808806350850', '1', '1', '2021-06-15 14:36:08', '2021-06-15 14:36:08');
INSERT INTO `shopping_cart` VALUES ('1404691041293631490', '17', '香辣味面筋辣条', 'l3.jpg', '14', '117', '1', '1381230808806350850', '1', '1', '2021-06-15 14:43:37', '2021-06-15 14:43:37');
INSERT INTO `shopping_cart` VALUES ('1404691642211565569', '14', '葵花籽奶油味瓜子', 'q3.jpg', '10', '34', '1', '1381230808806350850', '1', '0', '2021-06-15 14:46:00', '2021-06-15 14:46:00');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(32) NOT NULL COMMENT '用户ID',
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '用户密码',
  `authority` varchar(32) NOT NULL DEFAULT '普通用户' COMMENT '用户权限',
  `gender` varchar(4) NOT NULL DEFAULT '男' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(32) DEFAULT NULL COMMENT '手机号',
  `address` varchar(128) DEFAULT NULL COMMENT '居住地址',
  `status` int(16) NOT NULL DEFAULT '0' COMMENT '状态',
  `score` decimal(32,2) NOT NULL DEFAULT '0.00' COMMENT '积分',
  `version` int(4) NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `deleted` int(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1381230808806350850', 'Dirac', 'e10adc3949ba59abbe56e057f20f883e', '普通用户', '男', '2000-01-01', '12345678901@163.com', '12345678901', '上海滩', '1', '22.80', '14', '0', '2021-04-11 21:01:02', '2021-06-15 14:45:55');
INSERT INTO `user` VALUES ('1381546133359833090', 'Bob', 'e10adc3949ba59abbe56e057f20f883e', '普通用户', '男', '2021-04-12', '828282@qq.com', '13423452345', '桃花岛', '1', '0.00', '1', '0', '2021-04-12 17:54:01', '2021-04-12 17:54:01');
INSERT INTO `user` VALUES ('1386882384745111554', 'Mary', '202cb962ac59075b964b07152d234b70', '普通用户', '女', '2021-04-27', 'dhajhdkajh@124.com', '16276451826', '上海', '1', '0.00', '1', '0', '2021-04-27 11:18:22', '2021-04-27 11:18:22');
INSERT INTO `user` VALUES ('1397119091889344513', 'God', '202cb962ac59075b964b07152d234b70', '供应商', '女', '2015-05-16', '2197268176@qq.com', '17262628102', '上海浦东', '1', '0.00', '1', '0', '2021-05-25 17:15:23', '2021-05-25 17:15:23');
