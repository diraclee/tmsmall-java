package io.dirac.controller;

import io.dirac.result.Result;
import io.dirac.service.OrderListDtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order-list-dto")
public class OrderListDtoController {

    @Autowired
    private OrderListDtoService orderListDtoService;


    @GetMapping("/user/{userId}")
    public Result getBatchByUserId(@PathVariable(value = "userId") Long userId) {
        return Result.success(orderListDtoService.getBatchByUserId(userId));
    }
}
