package io.dirac.service;

import io.dirac.entity.OrderSurvey;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-14
 */
public interface OrderSurveyService extends IService<OrderSurvey> {


    public List<OrderSurvey> getByUserId(Long userId);
}
