package io.dirac.mapper;

import io.dirac.dto.SalesDto;
import io.dirac.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Dirac
 * @since 2021-04-11
 */
@Repository
public interface ProductMapper extends BaseMapper<Product> {

    @Select("SELECT product.*, sales.sales_volume from product, (select product_id, sum(purchase_quantity) as sales_volume from order_detail where deleted=0 group by product_id) as sales where deleted=0 and product.id=sales.product_id order by sales_volume desc")
    List<SalesDto> getSales();
}
