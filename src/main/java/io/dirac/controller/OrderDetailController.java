package io.dirac.controller;


import io.dirac.entity.OrderDetail;
import io.dirac.result.Result;
import io.dirac.service.OrderDetailService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Dirac
 * @since 2021-04-12
 */
@RestController
@RequestMapping("/order-detail")
public class OrderDetailController {

    @Autowired
    private OrderDetailService orderDetailService;

    @PostMapping
    public Result add(@Validated @RequestBody OrderDetail orderDetail) {
        orderDetailService.save(orderDetail);
        return Result.success("OK");
    }

    @PostMapping("/batch")
    public Result addBatch(@Validated @RequestBody List<OrderDetail> orderDetails) {
        orderDetailService.saveBatch(orderDetails);
        return Result.success(null);
    }

    @DeleteMapping("")
    public Result removeById(@RequestBody Long id) {
        orderDetailService.removeById(id);
        return Result.success("OK");
    }

    @DeleteMapping("/batch")
    public Result removeByIds(@RequestBody List<Long> ids) {
        orderDetailService.removeByIds(ids);
        return Result.success("OK");
    }
}
