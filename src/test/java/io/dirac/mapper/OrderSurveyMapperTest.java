package io.dirac.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.dirac.entity.OrderSurvey;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderSurveyMapperTest {

    @Autowired
    private OrderSurveyMapper orderSurveyMapper;

    @Test
    public void testGetAll() {
        QueryWrapper<OrderSurvey> wrapper = new QueryWrapper<>();
        orderSurveyMapper.selectList(wrapper).forEach(System.out::println);
    }

    @Test
    public void testUpdateById() {
        OrderSurvey orderSurvey = orderSurveyMapper.selectById(1384430347344355330L);
        orderSurveyMapper.updateById(orderSurvey);
    }
}
