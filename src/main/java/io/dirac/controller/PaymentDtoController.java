package io.dirac.controller;

import io.dirac.dto.PaymentDto;
import io.dirac.entity.OrderDetail;
import io.dirac.entity.OrderSurvey;
import io.dirac.result.Result;
import io.dirac.service.PaymentDtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order-dto")
public class PaymentDtoController {

    @Autowired
    private PaymentDtoService paymentDtoService;

    @GetMapping("/order/{orderId}")
    public Result getByOrderId(@PathVariable(value = "orderId") Long orderId) {
        return Result.success(paymentDtoService.getByOrderId(orderId));
    }
}
