package io.dirac.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.dirac.entity.OrderDetail;
import io.dirac.mapper.OrderDetailMapper;
import io.dirac.service.OrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-12
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Override
    public List<OrderDetail> getByOrderId(Long orderId) {
        QueryWrapper<OrderDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id", orderId);
        return orderDetailMapper.selectList(wrapper);
    }
}
