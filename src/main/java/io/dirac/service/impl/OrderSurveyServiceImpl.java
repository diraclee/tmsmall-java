package io.dirac.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.dirac.entity.OrderSurvey;
import io.dirac.mapper.OrderSurveyMapper;
import io.dirac.service.OrderSurveyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Dirac
 * @since 2021-04-14
 */
@Service
public class OrderSurveyServiceImpl extends ServiceImpl<OrderSurveyMapper, OrderSurvey> implements OrderSurveyService {

    @Autowired
    private OrderSurveyMapper orderSurveyMapper;

    @Override
    public List<OrderSurvey> getByUserId(Long userId) {
        QueryWrapper<OrderSurvey> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        wrapper.orderByDesc("gmt_created");
        return orderSurveyMapper.selectList(wrapper);
    }
}
